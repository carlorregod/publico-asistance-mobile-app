import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: string = '';

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private storage: Storage) {
    platform.ready().then(() => {
      statusBar.styleBlackOpaque();
      splashScreen.hide();
    });
    this.storage.get("user_rut").then((val) => {
      if (val) {
        this.rootPage = 'IndexPage';
      }
      else {
        this.rootPage = 'LoginPage';
      }
    });
  }
}

