import { Device } from '@ionic-native/device';
import { Sim } from '@ionic-native/sim';
import { DirectivesModule } from './../directives/directives.module';
import { PipesModule } from './../pipes/pipes.module';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { LoginProvider } from '../providers/login/login';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { IndexProvider } from '../providers/index/index';
import { CambioClaveProvider } from '../providers/cambio-clave/cambio-clave';
import { GoogleMaps } from '@ionic-native/google-maps';
import { Geolocation } from '@ionic-native/geolocation';
import { MapaProvider } from '../providers/mapa/mapa';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { HistorialProvider } from '../providers/historial/historial';
import { File } from '@ionic-native/file';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      menuType: 'push',
      platforms: {
        ios: {
          menuType: 'overlay',
        },
        android: {
          menuType: 'overlay',
        },
      },
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
        monthShortNames: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Agost', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayShortNames: ['Dom', 'Lun', 'Mar', 'Mier', 'Jue', 'Vie', 'Sab' ],
    }),
    PipesModule,
    IonicStorageModule.forRoot({
      name: 'asistanceDB',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    HttpClientModule,
    DirectivesModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    GoogleMaps,
    Geolocation,
    LoginProvider,
    IndexProvider,
    CambioClaveProvider,
    Sim,
    MapaProvider,
    AndroidPermissions,
    Device,
    HistorialProvider,
    File,
    
  ]
})
export class AppModule { }
