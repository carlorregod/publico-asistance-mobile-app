import { NgModule } from '@angular/core';
import { SoloRutDirective } from './solo-rut/solo-rut';
@NgModule({
	declarations: [SoloRutDirective],
	imports: [],
	exports: [SoloRutDirective]
})
export class DirectivesModule {}
