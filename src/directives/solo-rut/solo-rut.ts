import { Directive, HostListener } from '@angular/core';

/**
 * Generated class for the SoloRutDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[solo-rut]' // Attribute selector
})
export class SoloRutDirective {

  constructor() {
  }

  @HostListener('textInput', ['$event']) onKeyDown(event) {
    if(Number(event.data) >= 0 && Number(event.data) <= 9) {
      return;
    } else if (event.data.toLowerCase() === 'k') {
      return;
    } else {
      event.preventDefault();
    }
  }

}
