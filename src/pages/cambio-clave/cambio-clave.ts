import { CambioClaveProvider } from './../../providers/cambio-clave/cambio-clave';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the CambioClavePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cambio-clave',
  templateUrl: 'cambio-clave.html',
})
export class CambioClavePage {

  claveActual: string;
  nuevaClave: string;
  nuevaClaveConfirm: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public cambioClaveProvider: CambioClaveProvider, private storage: Storage, private loadingCtrl: LoadingController,
    private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CambioClavePage');
  }

  cambiarClave() {

    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Cargando, Porfavor espere un momento...'
    });

    if (this.claveActual.trim() === '') {
      this.mostrarToast('Campo Clave Actual no debe ir vacío');
      return;
    }
    if (this.nuevaClave.trim() === '') {
      this.mostrarToast('Campo Nueva Clave no debe ir vacío');
      return;
    }
    if (this.nuevaClaveConfirm.trim() === '') {
      this.mostrarToast('Campo Confirmar Nueva Clave no debe ir vacío');
      return;
    }
    if (this.nuevaClave.trim() !== this.nuevaClaveConfirm.trim()) {
      this.mostrarToast('Campos Nueva Clave y Confirmar Nueva Clave deben ser iguales');
      return;
    }
    if (this.claveActual.trim() === this.nuevaClaveConfirm.trim()) {
      this.mostrarToast('La Nueva Clave no debe ser igual a la Clave Actual');
      return;
    }
    loading.present();

    this.storage.get("user_rut").then((val) => {
      if (val) {
        this.cambioClaveProvider.cambiarClave(val, this.claveActual.trim(), this.nuevaClaveConfirm.trim()).then(data => {
          if (data.body.result === true) {
            loading.dismiss();
            this.mostrarToast('Su Clave ha sido modificada con éxito');
          } else {
            loading.dismiss();
            this.mostrarToast(data.body.message);
          }
        }).catch(error => {
          loading.dismiss();
          console.log(error);
          return;
        });
      }
    });
  }

  mostrarToast(texto: string) {
    let toast = this.toastCtrl.create({
      message: texto,
      duration: 2000,
      position: 'bottom'
    });

    toast.present();

  }
}
