import { HistorialProvider } from './../../providers/historial/historial';
import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, Platform, AlertController, ModalController } from 'ionic-angular';
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import { DatePipe, DecimalPipe } from '@angular/common';
import { File } from '@ionic-native/file';
import { AndroidPermissions } from '@ionic-native/android-permissions';

/**
 * Generated class for the HistorialPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-historial',
  templateUrl: 'historial.html',
})
export class HistorialPage {

  inputFecha: string;
  rutUsuario: string;
  mostrarError: boolean;

  initYear: string;
  initMonth: string;

  initYearMonth: string;

  registros: any[] = [];

  maxDate: string;

  mostrarRegistros: boolean;

  usuario: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage,
    private toastCtrl: ToastController, private historialProvider: HistorialProvider,
    private loadingCtrl: LoadingController, private platform: Platform, private file: File,
    private androidPermissions: AndroidPermissions, private alertCtrl: AlertController,
    private modalCtrl: ModalController) {
    this.mostrarError = false;

    this.storage.get('initYearMonth').then(val => {
      if (val) {
        this.initYear = val.year;
        this.initMonth = val.month;
        this.initYearMonth = this.initYear + '-' + ('0' + (this.initMonth)).slice(-2) + '-' + '01';
      }
    });
    this.storage.get('user_rut').then(val => {
      if (val) {
        this.rutUsuario = val;
      }
    });

    this.storage.get('basic_user').then(val => {
      if (val) {
        this.usuario = val;
      }
    });

    var MyDate = new Date();

    MyDate.setDate(MyDate.getDate());

    this.maxDate = MyDate.getFullYear() + '-'
      + ('0' + (MyDate.getMonth() + 1)).slice(-2) + '-'
      + ('0' + MyDate.getDate()).slice(-2);
  }

  getHistorial() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Cargando, Porfavor espere un momento...'
    });
    if (this.inputFecha !== '' && this.inputFecha !== undefined) {
      loading.present();
      let year: string = '';
      let month: string = '';

      year = this.inputFecha.split('-')[0];
      month = this.inputFecha.split('-')[1];
      this.historialProvider.getHistorial(this.rutUsuario, year, month).then(data => {
        if (data.body.result) {
          this.registros = data.body.registro;
          this.mostrarRegistros = true;
          this.mostrarError = false;
        } else {
          this.registros = [];
          this.mostrarRegistros = false;
          this.mostrarError = true;
        }
        loading.dismiss();
      }, error => {
        this.mostrarError = true;
        this.mostrarRegistros = false;
        loading.dismiss();
      });
    } else {
      loading.dismiss();
      this.mostrarToast('El campo de fecha no puede estar vacío para realizar la consulta.');
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HistorialPage');
  }

  mostrarToast(texto: string) {
    let toast = this.toastCtrl.create({
      message: texto,
      duration: 2500,
      position: 'bottom'
    });

    toast.present();
  }

  verMapa(coordX: string, coordY: string, tipoMarca: string) {
    this.navCtrl.push('VerMapaPage', { 'coordX': coordX, 'coordY': coordY, 'marca': tipoMarca });
  }


  verPermisosDeDescarga() {
    let lectura: any;
    let escritura: any;
    lectura = this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE).then(
      result => { return true },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.READ_EXTERNAL_STORAGE)
    );
    escritura = this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE).then(
      result => { return true },
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.WRITE_EXTERNAL_STORAGE)
    );
    if (lectura && escritura) {
      this.descargarPDF();
    }
  }


  descargarPDF() {
    let doc: any;
    doc = jsPDF('p', 'pt');
    // ***************************

    doc.setFontSize(14);
    let columns: any[];
    let rows: any[] = [];

    let fechaSplit: string[];
    fechaSplit = this.inputFecha.split('-');

    let fecha: string;
    fecha = '';
    fecha = fechaSplit[1] + '-' + fechaSplit[0];

    let text: string;
    doc.setFontSize(12);
    doc.setFontType('bold');
    text = 'HISTORIAL ' + fecha;
    doc.text(250, 70, text);
    doc.setFontType('normal');
    doc.setFontSize(10);

    let value: string = '';

    for (let index = 0; index < this.rutUsuario.length; index++) {
      if ((this.rutUsuario[index] !== '-') && (this.rutUsuario[index] !== '.')) {
        value += this.rutUsuario[index];
      }
    }
    let rutDefinitivo: string = '';
    let dv: string = value.substring(value.length - 1);
    let rut: string = value.substring(0, value.length - 1);
    if (!isNaN(Number(rut.toString()))) {
      rut = new DecimalPipe('en_US').transform(rut);
    }
    if (value.length > 1) {
      rutDefinitivo = rut.replace(/,/g, '.');
    }

    const itemIsquierda = {
      'RUT': `${rutDefinitivo}-${dv}`,
      'Nombre Trabajador': this.usuario.Nombres + ' ' + this.usuario.Apellido_P + ' ' + this.usuario.Apellido_M,
      'Fecha de ingreso': this.usuario.Fecha_ingreso,
      'ROL': this.usuario.Rol,
      'Teléfono asignado': this.usuario.Numero_celular
    };

    let columnasIzquierda: Array<any>;
    columnasIzquierda = [{
      title: 'DetailsIzquierda',
      dataKey: 'detailsIzquierda'
    }, {
      title: 'ValuesIzquierda',
      dataKey: 'valuesIzquierda'
    }];

    let filasDetallePolizaIzquierda: any;
    filasDetallePolizaIzquierda = Object.keys(itemIsquierda).map(key => {
      return { 'detailsIzquierda': key, 'valuesIzquierda': itemIsquierda[key] };
    });

    let optionsContainerIzquierda: any;
    optionsContainerIzquierda = {
      base: {},
      horizontalIzquierda: {
        drawHeaderRow: () => false,
        columnStyles: {
          detailsIzquierda: { fillColor: [238, 186, 0], textColor: 0, fontStyle: 'bold' }
        },
        margin: { right: 40 },
        startY: 95,
        styles: { cellPadding: 1, fontSize: 10 }
      }
    };

    doc.setPage(1 + doc.internal.getCurrentPageInfo().pageNumber - doc.autoTable.previous.pageCount);

    doc.text('DATOS DEL USUARIO', 40, 85);
    doc.autoTable(columnasIzquierda, filasDetallePolizaIzquierda, optionsContainerIzquierda['horizontalIzquierda']);

    for (let index = 0; index < this.registros.length; index++) {
      const element = this.registros[index];

      rows = [];

      columns = [
        'Ciclo',
        'Hora Entrada',
        'Hora Entrada Colación',
        'Hora Salida Colación',
        'Hora Salida'
      ];

      for (let i = 0; i < element.ciclos.length; i++) {
        let data: any;
        data = element.ciclos[i];
        const fila: any[] = [
          i + 1,
          data.entrada.hora !== '' ? data.entrada.hora : 'N/A',
          data.entradaColacion.hora !== '' ? data.entradaColacion.hora : 'N/A',
          data.salidaColacion.hora !== '' ? data.salidaColacion.hora : 'N/A',
          data.salida.hora !== '' ? data.salida.hora : 'N/A'
        ];
        rows.push(fila);
      }

      doc.text('FECHA: ' + new DatePipe('en_US').transform(element.fecha, 'dd-MM-yyyy'), 40, doc.autoTable.previous.finalY + 25);
      doc.autoTable(columns, rows, {
        startY: doc.autoTable.previous.finalY + 35,
        styles: { cellPadding: 1, fontSize: 10, halign: 'center' },
        headerStyles: { fillColor: [238, 186, 0], textColor: 0 }
      });

    }

    let pageCount: any;
    pageCount = doc.internal.getNumberOfPages();
    let fechaActual: string;
    fechaActual = '';
    fechaActual = new DatePipe('en_US').transform(this.maxDate, 'dd-MM-yyyy');
    for (let i = 0; i < pageCount; i++) {
      doc.setPage(i);
      doc.text(500, 820, 'Página ' + doc.internal.getCurrentPageInfo().pageNumber + ' de ' + pageCount);
      doc.text(40, 820, 'Fecha:   ' + fechaActual);
    }

    if (this.platform.is('cordova')) {
      let pdfOutput = doc.output();

      let buffer = new ArrayBuffer(pdfOutput.length);

      let array = new Uint8Array(buffer);

      for (var i = 0; i < pdfOutput.length; i++) {
        array[i] = pdfOutput.charCodeAt(i);
      }

      // For this, you have to use ionic native file plugin
      const directory = this.file.externalRootDirectory + 'Download/';

      const fileName = 'Historial ' + fecha + '.pdf';

      this.file.writeFile(directory, fileName, buffer, { replace: true })
        .then((success) => {
          console.log(success);

          this.presentAlert('Archivo Guardado!',
            'El archivo con los registros en formato PDF ha sido guardado satisfactoriamente. Para acceder a él debes ir a la carpeta Descargas de tu dispositivo.'
            , doc);
        })
        .catch((error) => console.log("Cannot Create File " + JSON.stringify(error)));
    } else {
      doc.save('Historial ' + fecha + '.pdf');
    }
  }

  presentAlert(titulo: string, texto: string, doc: any) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: texto,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            var blob = doc.output('blob', {type: 'application/pdf'});
            let pdfUrl = {pdfUrl: URL.createObjectURL(blob)};
            let modal = this.modalCtrl.create('VerPdfPage', pdfUrl);
            modal.present();
          }
        }
      ]
    });
    alert.present();
  }

}
