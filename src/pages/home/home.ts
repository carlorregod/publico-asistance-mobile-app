import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginProvider } from './../../providers/login/login';
import { Storage } from '@ionic/storage';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, FabContainer, ToastController } from 'ionic-angular';
import { Sim } from '@ionic-native/sim';
import { Device } from '@ionic-native/device';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  ultimaMarca: number;

  btnIngreso_in: boolean;
  btnIngreso_out: boolean;
  btnColacion_in: boolean;
  btnColacion_out: boolean;

  imei: string;
  numeroTelefonico: string;
  carrier: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private toastCtrl: ToastController,
    private sim: Sim, private storage: Storage, private loginProvider: LoginProvider, private device: Device) {
    this.getUltimoRegistroDeMarca();
    this.storage.set('imei_user', '');
    this.storage.set('carrier_user', '');
    this.storage.set('numero_telefonico_user', '');
    this.sim.requestReadPermission().then(
      () => {
        this.sim.hasReadPermission().then(
          (info) => {
            this.sim.getSimInfo().then(
              async (info) => {
                this.carrier = info.carrierName;
                this.numeroTelefonico = info.phoneNumber;
                this.storage.set('carrier_user', this.carrier);
                this.storage.set('numero_telefonico_user', this.numeroTelefonico);
                this.imei = this.device.uuid;
                this.storage.set('imei_user', this.imei);
              },
              (err) => console.log('Unable to get sim info: ', err)
            );
          }
        );
      },
      () => console.log('Permission denied')
    );
  }

  ionViewDidLoad() {
  }

  closeFabList(fab: FabContainer) {
    fab.close();
  }


  bloquearBotones() {
    switch (this.ultimaMarca) {
      case 0:
        this.btnIngreso_in = true;
        this.btnIngreso_out = false;
        this.btnColacion_in = false;
        this.btnColacion_out = false;
        break;
      case 1:
        this.btnIngreso_in = false;
        this.btnIngreso_out = true;
        this.btnColacion_in = true;
        this.btnColacion_out = false;
        break;
      case 2:
        this.btnIngreso_in = false;
        this.btnIngreso_out = true;
        this.btnColacion_in = false;
        this.btnColacion_out = true;
        break;
      case 3:
        this.btnIngreso_in = false;
        this.btnIngreso_out = true;
        this.btnColacion_in = false;
        this.btnColacion_out = false;
        break;
      default:
        break;
    }
  }

  irAMarcar(btn: string) {
    switch (btn) {
      case 'ingresoIn':
        if (!this.btnIngreso_in) {
          switch (this.ultimaMarca) {
            case 1:
            case 2:
            case 3:
              this.mostrarToast('Botón Ingreso no puede accionarse ya que usted ya ha marcado un ingreso previamente. Para volver a marcar un registro de entrada debe marcar una salida para concluir el ciclo.');
              break;
            default:
              break;
          }
        } else {
          this.navCtrl.push('MapaPage', { 'marca': '1' })
        }

        break;
      case 'ingresoOut':
        if (!this.btnIngreso_out) {
          switch (this.ultimaMarca) {
            case 0:
              this.mostrarToast('Botón Salida no puede accionarse ya que usted no ha marcado un ingreso previamente. Para marcar un registro de salida debe marcar un ingreso previo para realizar la acción.');
              break;
            default:
              break;
          }
        } else {
          this.navCtrl.push('MapaPage', { 'marca': '0' })
        }

        break;
      case 'colacionIn':
        if (!this.btnColacion_in) {
          switch (this.ultimaMarca) {
            case 0:
              this.mostrarToast('Botón Entrada a Colación no puede accionarse ya que usted no ha marcado un ingreso previamente. Para marcar un registro de ingreso a colación debe marcar un ingreso previo para realizar la acción.');
              break;
            case 2:
              this.mostrarToast('Botón Entrada a Colación no puede accionarse ya que usted ha marcado un ingreso previamente. Para marcar un registro de entrada a colación debe marcar una salida de jornada previa y luego un nuevo ingreso de jornada para realizar la acción.');
              break;
            case 3:
              this.mostrarToast('Botón Entrada a Colación no puede accionarse ya que usted ya ha marcado un ingreso previamente. Para marcar un registro de entrada a colación debe marcar una salida de jornada previa y luego un nuevo ingreso de jornada para realizar la acción.');
              break;
            default:
              break;
          }
        } else {
          this.navCtrl.push('MapaPage', { 'marca': '2' })
        }

        break;
      case 'colacionOut':
        if (!this.btnColacion_out) {
          switch (this.ultimaMarca) {
            case 0:
              this.mostrarToast('Botón Salida de Colación no puede accionarse ya que usted no ha marcado un ingreso previamente. Para marcar un registro de ingreso a colación debe marcar un ingreso previo para realizar la acción.');
              break;
            case 1:
              this.mostrarToast('Botón Salida de Colación no puede accionarse ya que usted no ha marcado un ingreso a colación previamente. Para marcar un registro de salida debe marcar un ingreso a colación previo para realizar la acción.');
              break;
            case 3:
              this.mostrarToast('Botón Salida de Colación no puede accionarse ya que usted ya ha marcado una salida de colación previamente. Para marcar un registro de entrada a colación debe marcar una salida de jornada previa y luego un nuevo ingreso de jornada para realizar la acción.');
              break;
            default:
              break;
          }
        } else {
          this.navCtrl.push('MapaPage', { 'marca': '3' })
        }

        break;

      default:
        break;
    }
  }

  mostrarToast(texto: string) {
    let toast = this.toastCtrl.create({
      message: texto,
      duration: 5000,
      position: 'bottom'
    });

    toast.present();

  }

  getUltimoRegistroDeMarca() {
    this.storage.get("user_rut").then((val) => {
      if (val) {
        this.loginProvider.lastRegistry(val).then(data => {
          this.ultimaMarca = data.body;
          this.bloquearBotones();
        }).catch(error => {
        });
      }
    });
  }

}
