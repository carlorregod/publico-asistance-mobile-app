import { IndexProvider } from './../../providers/index/index';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Nav, AlertController, LoadingController } from 'ionic-angular';
import { Storage } from '@ionic/storage';


/**
 * Generated class for the IndexPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-index',
  templateUrl: 'index.html',
})
export class IndexPage {

  @ViewChild(Nav) nav: Nav;

  usuario: any = null;
  rutUsuario: string = '';

  pages: any[] = [
    { title: 'Marcar Asistencia', component: 'HomePage', icon: 'ios-locate-outline' },
    { title: 'Ver Historial de Asistencias', component: 'HistorialPage', icon: 'ios-list-box-outline' },
    { title: 'Cambiar Clave Acceso', component: 'CambioClavePage', icon: 'ios-key-outline' }
  ];

  principalPage: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private alertCtrl: AlertController,
    private indexProvider: IndexProvider,
    private loadingCtrl: LoadingController) {

    this.principalPage = this.pages[0].component;
    this.obtenerDatosUsuario();
  }

  ionViewDidLoad() {
  }

  obtenerDatosUsuario() {
    this.storage.get("user_rut").then((val) => {
      if (val) {
        this.rutUsuario = val;
        this.indexProvider.obtenerDatosUsuario(this.rutUsuario).then(data => {
          if (data.body.result === true) {
            this.usuario = data.body.user;
            this.storage.set('basic_user', this.usuario);
          }
        }).catch(error => {
        });
      } else {
        this.principalPage = 'LoginPage';
      }
    });

  }

  openPage(page) {
    if (page.component === 'HistorialPage') {
      this.consultarUltimoRegistro();
    } else {
      this.nav.setRoot(page.component);
    }
  }

  cerrarSesion() {
    let alert = this.alertCtrl.create({
      title: 'Cerrar Sesión',
      message: '¿Estás seguro que deseas cerrar sesión?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            this.storage.clear();
            this.navCtrl.setRoot('LoginPage');
          }
        }
      ]
    });
    alert.present();
  }

  consultarUltimoRegistro() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Cargando, Porfavor espere un momento...'
    });
    loading.present();
    this.indexProvider.obtenerUltimoRegistro(this.rutUsuario).then(data => {
      if(data.body.result) {
        loading.dismiss();
        this.storage.set('initYearMonth', {'year': data.body.year, 'month': data.body.month});
        this.nav.setRoot('HistorialPage');
      } else {
        loading.dismiss();
        this.presentAlert('Sin registros', 'Estimado usuario, no tenemos registro de marcas previas realizadas. Porfavor, para ingresar a esta sección realice un registro de marca.');
      }
    }).catch(error => {
        loading.dismiss();
    });

  }

  presentAlert(titulo: string, texto: string) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: texto,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

}
