import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ToastController } from 'ionic-angular';
import { LoginProvider } from '../../providers/login/login';
import { Storage } from '@ionic/storage';
import { DecimalPipe } from '@angular/common';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  rut: string = '';
  clave: string = '';
  rutValidado: boolean;

  constructor(public storage: Storage, public navCtrl: NavController, public navParams: NavParams, private loginProvider: LoginProvider,
    private loadingCtrl: LoadingController, private toastCtrl: ToastController) {
  }

  ionViewDidLoad() {
  }

  login() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Cargando, Porfavor espere un momento...'
    });

    if (this.rut.trim() === '') {
      this.mostrarToast('Campo RUT no debe ir vacío');
      return;
    }
    if (this.clave.trim() === '') {
      this.mostrarToast('Campo Clave no debe ir vacío');
      return;
    }
    this.validarRut();

    if (this.rutValidado) {
      loading.present();

      this.formatearRut(0);

      this.loginProvider.login(this.rut.trim(), this.clave.trim()).then(data => {
        if (data.body.result === false) {
          loading.dismiss();
          this.mostrarToast('RUT y/o Clave incorrectos.');
          this.clave = '';
        } else {
          loading.dismiss();
          this.storage.set("user_rut", data.body.rut);
          this.navCtrl.setRoot('IndexPage');
          return;
        }
      }).catch(error => {
        loading.dismiss();
      });
    }
  }

  mostrarToast(texto: string) {
    let toast = this.toastCtrl.create({
      message: texto,
      duration: 2500,
      position: 'bottom'
    });

    toast.present();

  }

  formatearRut(tipo: number) {
    if (tipo === 1) {
      if (this.rut !== '' && this.rut !== null) {
        let value: string = '';

        for (let index = 0; index < this.rut.length; index++) {
          if ((this.rut[index] !== '-') && (this.rut[index] !== '.')) {
            value += this.rut[index];
          }
        }

        let dv: string = value.substring(value.length - 1);
        let rut: string = value.substring(0, value.length - 1);
        if (!isNaN(Number(rut.toString()))) {
          rut = new DecimalPipe('en_US').transform(rut);
        }
        if (value.length > 1) {
          let rutDefinitivo: string = '';
          rutDefinitivo = rut.replace(/,/g, '.');
          this.rut = `${rutDefinitivo}-${dv}`;
        }
      }
    } else if (tipo === 0) {
      if (this.rut !== '' && this.rut !== null) {
        let value: string = '';
        for (let index = 0; index < this.rut.length; index++) {
          if ((this.rut[index] !== '-') && (this.rut[index] !== '.')) {
            value += this.rut[index];
          }
        }
        this.rut = value;
      }
    }
  }

  validarRut() {
    if (this.rut !== null && this.rut !== '') {
      if (this.rut.length >= 9) {
        let re: any;
        re = /\./gi;
        let RUT: any;
        RUT = this.rut.replace(re, '');

        let hayGuion: boolean;
        hayGuion = false;

        if (RUT.substring(RUT.length - 2).substring(0, 1) === '-') {
          hayGuion = true;
        }

        if (!hayGuion) {
          RUT = RUT.substring(0, RUT.length - 1) + '-' + RUT.substring(RUT.length - 1);
        }

        if (this.checkRUT(RUT) === false) {
          this.mostrarToast('Rut ingresado no es válido.');
          this.rutValidado = false;
        } else {
          this.rutValidado = true;
        }
      }
    }
  }

  checkRUT(Objeto: any) {
    let tmpstr = '';
    let intlargo: any;
    intlargo = Objeto;
    if (intlargo.length > 0) {
      let crut: any;
      crut = Objeto;
      let largo: any;
      largo = crut.length;
      if (largo < 2) {
        return false;
      }
      for (let i = 0; i < crut.length; i++) {
        if (crut.charAt(i) !== ' ' && crut.charAt(i) !== '.' && crut.charAt(i) !== '-') {
          tmpstr = tmpstr + crut.charAt(i);
        }
      }
      let rut: any;
      let dv: any;
      crut = tmpstr;
      largo = crut.length;
      if (largo > 2) {
        rut = crut.substring(0, largo - 1);
      } else {
        rut = crut.charAt(0);
      }
      dv = crut.charAt(largo - 1);
      if (rut == null || dv == null) {
        return 0;
      }
      let dvr;
      let suma: any;
      suma = 0;
      let mul: any;
      mul = 2;
      for (let i = rut.length - 1; i >= 0; i--) {
        suma = suma + rut.charAt(i) * mul;
        if (mul === 7) {
          mul = 2;
        } else {
          mul++;
        }
      }
      let res: any;
      let dvi: any;
      res = suma % 11;
      if (res === 1) {
        dvr = 'k';
      } else if (res === 0) {
        dvr = '0';
      } else {

        dvi = 11 - res;
        dvr = dvi;
      }
      if (dvr.toString().toLowerCase() !== dv.toString().toLowerCase()) {
        return false;
      }
      return true;
    }
  }

}
