import { Storage } from '@ionic/storage';
import { MapaProvider } from './../../providers/mapa/mapa';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController, Loading, Alert, AlertController } from 'ionic-angular';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { Sim } from '@ionic-native/sim';
import { Device } from '@ionic-native/device';

declare var google;

/**
 * Generated class for the MapaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage( { name: 'MapaPage' })
@Component({
  selector: 'page-mapa',
  templateUrl: 'mapa.html',
})
export class MapaPage {

  map: any;
  nombreMarca: string = '';

  rut: string;
  coordX: number;
  coordY: number;
  tipoMarca: number;
  imei: string;
  carrier: string;
  numeroTelefonico: string;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private geolocation: Geolocation,
    private menuCtrl: MenuController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private mapaProvider: MapaProvider,
    private storage: Storage,
    private sim: Sim,
    private device: Device) {
    this.menuCtrl.swipeEnable(false);
    this.tipoMarca = this.navParams.get('marca');

    switch (this.navParams.get('marca')) {
      case '0':
        this.nombreMarca = 'Salida';
        break;
      case '1':
        this.nombreMarca = 'Entrada';
        break;
      case '2':
        this.nombreMarca = 'Entrada Colación';
        break;
      case '3':
        this.nombreMarca = 'Salida Colación';
      default:
        break;
    }

    this.storage.get('user_rut').then(val => {
      if (val) {
        this.rut = val;
      }
    });
    this.storage.get('imei_user').then(val => {
      if (val) {
        this.imei = val;
      }
    });
    this.storage.get('carrier_user').then(val => {
      if (val) {
        this.carrier = val;
      }
    });
    this.storage.get('numero_telefonico_user').then(val => {
      if (val) {
        this.numeroTelefonico = val;
      }
    });
    if ((this.imei !== '' && this.imei !== undefined) && this.carrier !== '' && this.numeroTelefonico != '') {

    } else {
      this.sim.requestReadPermission().then(
        () => {
          this.sim.hasReadPermission().then(
            (info) => {
              this.sim.getSimInfo().then(
                async (info) => {
                  this.carrier = info.carrierName;
                  this.numeroTelefonico = info.phoneNumber;
                  this.storage.set('carrier_user', this.carrier);
                  this.storage.set('numero_telefonico_user', this.numeroTelefonico);
                  this.imei = this.device.uuid;
                  this.storage.set('imei_user', this.imei);
                },
                (err) => console.log('Unable to get sim info: ', err)
              );
            }
          );
        },
        () => console.log('Permission denied')
      );
    }
  }

  ionViewDidLoad() {
    this.getPosition();
  }

  ionViewWillLeave() {
    this.menuCtrl.swipeEnable(true);
  }

  getPosition(): any {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Cargando, Porfavor espere un momento...'
    });
    loading.present();
    this.geolocation.getCurrentPosition()
      .then(response => {
        this.loadMap(response, loading);
      })
      .catch(error => {
        loading.dismiss();
        this.navCtrl.setRoot('HomePage');
        console.log(error);
      })
  }

  loadMap(position: Geoposition, loading: Loading) {
    let latitude = position.coords.latitude;
    let longitude = position.coords.longitude;
    this.coordX = latitude;
    this.coordY = longitude;

    // create a new map by passing HTMLElement
    let mapEle: HTMLElement = document.getElementById('map');

    // create LatLng object
    let myLatLng = { lat: latitude, lng: longitude };

    // create map
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 18
    });

    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      let marker = new google.maps.Marker({
        position: myLatLng,
        map: this.map,
        title: 'Ubicación Actual'
      });
      mapEle.classList.add('show-map');
    });
    loading.dismiss();
  }

  marcarAsistencia() {
    let alert = this.alertCtrl.create({
      title: 'Marcar Asistencia',
      message: '¿Estás seguro que deseas marcar asistencia en este punto?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Aceptar',
          handler: () => {
            let loading = this.loadingCtrl.create({
              spinner: 'crescent',
              content: 'Cargando, Porfavor espere un momento...'
            });
            loading.present();
            if (this.imei !== '' && this.imei !== undefined) {
              this.mapaProvider.registrarMarca(this.rut,
                this.coordX,
                this.coordY,
                this.tipoMarca,
                this.imei,
                this.carrier !== '' ? this.carrier : '',
                this.numeroTelefonico !== '' ? this.formateoNumeroTelefonico(this.numeroTelefonico) : '').then(data => {
                  loading.dismiss();
                  if (data.body.result) {
                    this.presentAlert('Operación Realizada', 'El registro de marca ha sido guardado con éxito.');
                  } else {
                    this.presentAlert('Error', 'Ha ocurrido un error al registrar el marcado de asistencia. Intente más tarde.');
                  }
                }).catch(error => {
                  loading.dismiss();
                  this.presentAlert('Error', 'Ha ocurrido un error al registrar el marcado de asistencia. Intente más tarde.');
                });
            } else {
              loading.dismiss();
              this.presentarErrorDePermisos();
            }
          }
        }
      ]
    });
    alert.present();
  }

  formateoNumeroTelefonico(numero: string): string {
    if (numero === undefined) {
      return '';
    }
    let val: string;
    val = numero.toString().trim();
    let val2: string = val.replace('+', '')
    let valFinal: string;
    valFinal = val2.replace(/ +/g, "");
    return valFinal;
  }

  presentarErrorDePermisos() {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: 'No es posible realizarse esta acción debido a que faltan permisos para obtener datos del dispositivo.',
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            console.log('Cancel clicked');
            this.navCtrl.setRoot('HomePage');
          }
        }
      ]
    });
    alert.present();
  }

  presentAlert(titulo: string, texto: string) {
    let alert = this.alertCtrl.create({
      title: titulo,
      subTitle: texto,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            this.navCtrl.setRoot('HomePage');
          }
        }
      ]
    });
    alert.present();
  }



}
