import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, MenuController, LoadingController, Loading } from 'ionic-angular';

declare var google;

@IonicPage()
@Component({
  selector: 'page-ver-mapa',
  templateUrl: 'ver-mapa.html',
})
export class VerMapaPage {

  map: any;

  nombreMarca: string = '';
  coordX: string;
  coordY: string;
  tipoMarca: number;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private menuCtrl: MenuController,
    private loadingCtrl: LoadingController) {
    this.menuCtrl.swipeEnable(false);
    this.tipoMarca = this.navParams.get('marca');

    switch (this.navParams.get('marca')) {
      case '0':
        this.nombreMarca = 'Salida';
        break;
      case '1':
        this.nombreMarca = 'Entrada';
        break;
      case '2':
        this.nombreMarca = 'Entrada Colación';
        break;
      case '3':
        this.nombreMarca = 'Salida Colación';
      default:
        break;
    }

    this.coordX = this.navParams.get('coordX');
    this.coordY = this.navParams.get('coordY');
  }

  ionViewDidLoad() {
    this.getPosition();
  }

  ionViewWillLeave() {
    this.menuCtrl.swipeEnable(true);
  }

  getPosition(): any {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Cargando, Porfavor espere un momento...'
    });
    loading.present();
    this.loadMap(loading);
  }

  loadMap(loading: Loading) {

    // create a new map by passing HTMLElement
    let mapEle: HTMLElement = document.getElementById('map');

    // create LatLng object
    let myLatLng = { lat: Number(this.coordX), lng: Number(this.coordY) };

    // create map
    this.map = new google.maps.Map(mapEle, {
      center: myLatLng,
      zoom: 18
    });

    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      let marker = new google.maps.Marker({
        position: myLatLng,
        map: this.map,
        title: 'Ubicación Actual'
      });
      mapEle.classList.add('show-map');
    });
    loading.dismiss();
  }

}
