import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerPdfPage } from './ver-pdf';
import { PdfViewerComponent } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [
    VerPdfPage,
    PdfViewerComponent
  ],
  imports: [
    IonicPageModule.forChild(VerPdfPage),
  ],
})
export class VerPdfPageModule {}
