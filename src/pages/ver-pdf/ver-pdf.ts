import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PdfViewerComponent } from 'ng2-pdf-viewer';

/**
 * Generated class for the VerPdfPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ver-pdf',
  templateUrl: 'ver-pdf.html',
})
export class VerPdfPage {

  pdfUrl: String;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VerPdfPage');
    this.pdfUrl = this.navParams.get('pdfUrl');
  }

  closeModal() {
    this.navCtrl.pop();
  }

}
