import { NgModule } from '@angular/core';
import { RutFormatterPipe } from './rut-formatter/rut-formatter';
@NgModule({
	declarations: [RutFormatterPipe],
	imports: [],
	exports: [RutFormatterPipe]
})
export class PipesModule {}
