import { Pipe, PipeTransform } from '@angular/core';
import { DecimalPipe } from '@angular/common';

/**
 * Generated class for the RutFormatterPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'rutFormatter',
})
export class RutFormatterPipe implements PipeTransform {
  transform(value: string, ...args) {
    let dv: string = value.substring(value.length - 1);
    let rut: string = value.substring(0, value.length - 1);
    rut = new DecimalPipe('en_US').transform(rut);
    let rutDefinitivo = rut.replace(/,/g, '.');
    return `${rutDefinitivo}-${dv}`;
  }
}
