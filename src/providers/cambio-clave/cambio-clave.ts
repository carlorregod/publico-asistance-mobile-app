import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the CambioClaveProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CambioClaveProvider {

  constructor(public http: HttpClient) {
    console.log('Hello CambioClaveProvider Provider');
  }

  cambiarClave(rut: string, clave: string, nuevaClave: string) {
    return this.http.put('https://asis-service.tk/api/users/newpassword', { rut: rut, password: clave, newPassword: nuevaClave }, { observe: 'response' }).toPromise().then(data => {
      return data;
    }).catch(error => {
      return error;
    })
  }

}
