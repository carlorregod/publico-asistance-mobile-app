import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the HistorialProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HistorialProvider {

  constructor(public http: HttpClient) {
    console.log('Hello HistorialProvider Provider');
  }

  getHistorial(rut: string, year: string, month: string) {
    return this.http.post('https://asis-service.tk/api/users/serchRegistry', { rut: rut, year: year, month: month }, { observe: 'response' }).toPromise().then(data => {
      return data;
    }).catch(error => {
      return error;
    })
  }

}
