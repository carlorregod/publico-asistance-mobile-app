import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the IndexProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class IndexProvider {

  constructor(public http: HttpClient) {
    console.log('Hello IndexProvider Provider');
  }

  obtenerDatosUsuario(rut: string) {
    return this.http.get(`https://asis-service.tk/api/users/${rut}`, { observe: 'response' }).toPromise().then(data => {
      return data;
    }).catch(error => {
      return error;
    })
  }

  obtenerUltimoRegistro(rut: string) {
    return this.http.get(`https://asis-service.tk/api/users/firstRegistry/${rut}`, { observe: 'response' }).toPromise().then(data => {
      return data;
    }).catch(error => {
      return error;
    })
  }

}
