import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the LoginProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LoginProvider {

  constructor(public http: HttpClient) {
    console.log('Hello LoginProvider Provider');
  }

  login(rut: string, clave: string) {
    return this.http.post('https://asis-service.tk/api/users/login', { rut: rut, password: clave }, { observe: 'response' }).toPromise().then(data => {
      return data;
    }).catch(error => {
      return error;
    })
  }

  lastRegistry(rut: string) {
    
    return this.http.get('https://asis-service.tk/api/users/lastRegistry/' + rut, { observe: 'response' }).toPromise().then(data => {
      return data;
    }).catch(error => {
      return error;
    })
  }

}
