import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the MapaProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MapaProvider {

  constructor(public http: HttpClient) {
    console.log('Hello MapaProvider Provider');
  }

  registrarMarca(rut, coordX, coordY, id_marcado, imei, compania, num_celular) {
    return this.http.post('https://asis-service.tk/api/users/registry', {
      rut: rut,
      coordX: coordX,
      coordY: coordY,
      id_marcado: id_marcado,
      imei: imei,
      compania: compania,
      num_celular: num_celular
    }, { observe: 'response' }).toPromise().then(data => {
      return data;
    }).catch(error => {
      return error;
    })
  }

}
